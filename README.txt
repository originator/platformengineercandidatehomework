Hello, and thanks for your interest in the Platform Engineer position at Originator! This file
will give you the necessary instructions for getting started on the next step of our hiring
process: a short homework assignment where you can demonstrate a few of the real-world skills
the position will require.

At a high level, the assignment is to create a small, working Unity project that renders a
3D structure that is procedurally generated from a data file. The front-end portion of
the code is provided as part of this repo, and you'll be responsible for setting up the
project and building the back end. This mirrors how the Platform Engineer will fit into
our team.

-----------------------------------------------------------------------------------------

What you'll need:

1. Unity. The code and assets provided were tested in Unity 2020.3.12f1, but should work in
any reasonably recent release. You'll be responsible for creating the project and its
file / folder structure, as well as setting up a scene and launching the various code
pieces from it.

2. The provided C# file, FrontEndAPI.cs. This contains all the code that handles the
user-facing graphical functionality. The comments in the file will guide you through the
simple steps to set it up and wire it to your back end.

3. The assets provided in the "Resources" folder. The front end code expects to be able to
load these via Unity's Resources.Load() method, so you should place them accordingly in
your project structure.

4. The data file. Your code should retrieve it, at runtime, from the following URL:
http://www.originatorkids.com/hiring/hex_cells.txt. It should then parse the data into
whatever structure / format you think best, and feed it to the front end for rendering.

-----------------------------------------------------------------------------------------

Data file format:

The data file is a real file that is used by one of our products. It defines the geometry of
a large, polygonal sphere that is composed of 1430 hexagons and 12 pentagons. Each row of the
data file provides the data necessary for one of these polygons, as well as some metadata
needed by the game. Although you won't be using all of the metadata fields for this assignment,
it's smart to plan ahead for possible ways your code might need to grow and be extended later.

Here is the first data row as an example. This is the pentagon at the north pole of the sphere:

0 0 1 0,39,0 1.138,38.888,1.567 1.842,38.888,-0.598 0,38.888,-1.936 -1.842,38.888,-0.598 -1.138,38.888,1.567 1,2,3,4,5

As you can see, the different fields are space-delimited. Here's what they mean:

1. A ring number (0, in this case). The polygons roughly form rings that go latitudinally
around the sphere, from the pentagon at the "north pole" to one at the "south pole."

2. A ring position (0, in this case). This denotes the position this polygon has within its ring.

3. A sector number (1, in this case). As well as rings, the sphere is divided into 48 "sectors,"
which are chunks of the sphere that are gradually added as the game progresses.

4. A "canonical center" (0,39,0). This is a point that the game treats as the polygon's center.
As these are not precisely regular hexagons, this is only an approximation.

5. Five or six space-delimited vertex points. These are what you'll need to send to the front
end for rendering, along with the canonical center. It's important to send them to the front
end in the same order as in the file.

6. The last token contains the five or six cells that neighbor this one, which are given in
the data file as comma-delimited numeric indices.

Note that the first row of the file is the version number of the data format. You won't need it
for this assignment.

-----------------------------------------------------------------------------------------

Things to note:

- The platform team's code is re-used across many products and across many years of time. It
needs to demonstrate top-notch quality in terms of clarity, organization, adaptability, and
the other metrics that are commonly associated with "good" code. It needs to be forward-looking,
in that it positions itself for expected future expansion without building more than necessary,
as well as foreseeing problems and avoiding them before they arise.

- The sphere has a radius of 39 units. So, in the interest of getting a good view of it at runtime,
it might be best to position the camera further back from its default position. In testing,
we found a position of (0, 0, -80) to be pretty good.

- Rather than checking your project back into the repository it came from, please send the
final version to us by zipping up the project folder and emailing it back. Or, if you'd like
to use source control as you work, feel free to set up your own repo for us to look at.
